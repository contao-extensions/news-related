<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2024 Leo Feyer
 *
 * @package   contao-news-related
 * @author    Fast & Media | Christian Schmidt <dev@fast-media.net>
 * @license   LGPL
 * @copyright Fast & Media 2013-2024 <https://www.fast-media.net>
 */


/**
 * Frontend modules
 */
$GLOBALS['FE_MOD']['news']['news_related'] = 'ModuleNewsRelated';

/**
 * Hooks
 */
if (TL_MODE == 'FE')
{
	$GLOBALS['TL_HOOKS']['parseTemplate'][] = array('NewsRelated', 'newsreaderChange');
}
