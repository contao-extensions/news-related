<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2024 Leo Feyer
 *
 * @package   contao-news-related
 * @author    Fast & Media | Christian Schmidt <dev@fast-media.net>
 * @license   LGPL
 * @copyright Fast & Media 2013-2024 <https://www.fast-media.net>
 */


/**
 * Register the namespaces
 */
ClassLoader::addNamespaces(array
(
	'NewsRelated',
));


/**
 * Register the classes
 */
ClassLoader::addClasses(array
(
	// Classes
	'NewsRelated\NewsRelatedHelper' => 'system/modules/news-related/classes/NewsRelatedHelper.php',

	// Modules
	'NewsRelated\ModuleNewsRelated' => 'system/modules/news-related/modules/ModuleNewsRelated.php',
	'NewsRelated\NewsRelated'       => 'system/modules/news-related/modules/NewsRelated.php',
));


/**
 * Register the templates
 */
TemplateLoader::addFiles(array
(
	'news_related'    => 'system/modules/news-related/templates/news',
	'mod_newsrelated' => 'system/modules/news-related/templates/modules',
	'mod_newsreader'  => 'system/modules/news-related/templates/modules',
));
