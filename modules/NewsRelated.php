<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2024 Leo Feyer
 *
 * @package   contao-news-related
 * @author    Fast & Media | Christian Schmidt <dev@fast-media.net>
 * @license   LGPL
 * @copyright Fast & Media 2013-2024 <https://www.fast-media.net>
 */


/**
 * Run in a custom namespace, so the class can be replaced
 */
namespace NewsRelated;

class NewsRelated extends \Frontend
{

	public function newsreaderChange($objTemplate)
	{
		// Choose template
		if (strpos($objTemplate->getName(), 'mod_newsreader') === 0)
		{
			// Limit
			if($objTemplate->related_numberOfItems > 0 && $objTemplate->related_numberOfItems <= 50)
			{
				$limit = $objTemplate->related_numberOfItems;
			}
			else
			{
				return '';
			}

			$this->import('NewsRelatedHelper', 'Helper');
			$objArticle = $this->Helper->getRelated($objTemplate->news_archives, $objTemplate->related_match, $objTemplate->related_priority, $limit);

			if($objArticle)
			{
				// Get page
				global $objPage;

				// Defaults
				$arrArticles = array();
				while($objArticle->next())
				{
					$arrPic = '';

					// Add an image
					if ($objArticle->addImage && $objArticle->singleSRC != '')
					{
						$objModel = \FilesModel::findByUuid($objArticle->singleSRC);

						if ($objModel === null)
						{
							if (!\Validator::isUuid($objArticle->singleSRC))
							{
								$objTemplate->text = '<p class="error">'.$GLOBALS['TL_LANG']['ERR']['version2format'].'</p>';
							}
						}
						elseif (is_file(TL_ROOT . '/' . $objModel->path))
						{
							// Do not override the field now that we have a model registry (see #6303)
							$arrArticle = $objArticle->row();

							// Override the default image size
							if ($objTemplate->thumbSize != '')
							{
								$size = deserialize($objTemplate->thumbSize);

								if ($size[0] > 0 || $size[1] > 0 || is_numeric($size[2]))
								{
									$arrArticle['size'] = $objTemplate->thumbSize;
					 			}
							}

							if (!$objArticle->overwriteMeta)
							{
								$arrMeta = deserialize($objModel->meta);
								$arrArticle['alt'] = $arrMeta[$GLOBALS['TL_LANGUAGE']]['alt'];
								$objArticle->alt = $arrArticle['alt'];
								$arrArticle['caption'] = $arrMeta[$GLOBALS['TL_LANGUAGE']]['caption'];
								$objArticle->caption = $arrArticle['caption'];
								$arrArticle['title'] = $arrMeta[$GLOBALS['TL_LANGUAGE']]['title'];
							}

							$arrArticle['singleSRC'] = $objModel->path;
							$this->addImageToTemplate($objTemplate, $arrArticle);

							$arrPic = array(
								'picture' => $objTemplate->picture,
								'alt' => $objArticle->alt,
								'fullsize' => $objArticle->fullsize,
								'caption' => $objArticle->caption
							);
						}
					}

					// Shorten the teaser
					$teaser = strip_tags($objArticle->teaser,array('<strong>','<a>'));

					if(strlen($teaser) > 120)
					{
						$teaser = \StringUtil::substrHtml($teaser, 120) . '...';
					}

					$objArchive = \Database::getInstance()->prepare("SELECT tstamp, title, jumpTo FROM tl_news_archive WHERE id=?")->execute($objArticle->pid);

					if (($objTarget = \PageModel::findByPk($objArchive->jumpTo)) !== null)
					{
						$url = ampersand($this->generateFrontendUrl($objTarget->row(), ((isset($GLOBALS['TL_CONFIG']['useAutoItem']) && $GLOBALS['TL_CONFIG']['useAutoItem']) ?  '/' : '/items/') . ((!$GLOBALS['TL_CONFIG']['disableAlias'] && $objArticle->alias != '') ? $objArticle->alias : $objArticle->id)));
					}

					$title = specialchars(sprintf($GLOBALS['TL_LANG']['MSC']['readMore'], $objArticle->headline), true);

					$this->news_metaFields = $objTemplate->news_metaFields;
					$arrMeta = $this->getMetaFields($objArticle);

					$more = sprintf
					(
						'<a href="%s" title="%s">%s%s</a>',
						$url,
						$title,
						$GLOBALS['TL_LANG']['MSC']['more'],
						($blnIsReadMore ? ' <span class="invisible">'.$objArticle->headlines.'</span>' : '')
					);

					//Newsdaten hinzufügen
					$arrArticles[] = array
					(
						'headline' => $objArticle->headline,
						'id' => $objArticle->id,
						'subheadline' => $objArticle->subheadline,
						'teaser' => $teaser,
						'more' => $more,
						'image' => $arrPic,
						'url' => $url,
						'title' => $title,
						'numberOfComments' => $arrMeta['ccount'],
						'commentCount' => $arrMeta['comments'],
						'date' => $arrMeta['date'],
						'timestamp' => $objArticle->date,
						'datetime' => date('Y-m-d\TH:i:sP', $objArticle->date)
					);
				}

				// assign articles
				$objTemplate->info = $GLOBALS['TL_LANG']['MSC']['related_info'];
				$objTemplate->related_headline = $GLOBALS['TL_LANG']['MSC']['related_headline'];
				if(!empty($arrArticles) && is_array($arrArticles))
				{
					$objTemplate->newsRelated = $arrArticles;
				}
				else { return ''; }
			}
		}
	}

	/**
	 * Return the meta fields of a news article as array
	 * @param object
	 * @return array
	 */
	protected function getMetaFields($objArticle)
	{
		$meta = deserialize($this->news_metaFields);

		if (!is_array($meta))
		{
			return array();
		}

		/** @var \PageModel $objPage */
		global $objPage;

		$return = array();

		foreach ($meta as $field)
		{
			switch ($field)
			{
				case 'date':
					$return['date'] = \Date::parse($objPage->datimFormat, $objArticle->date);
					break;

				case 'author':
					/** @var \UserModel $objAuthor */
					if ($objArticle->author)
					{
						$strAuthor = \Database::getInstance()->prepare("SELECT name FROM tl_user	WHERE id=?")->execute($objArticle->author)->name;
						$return['author'] = $GLOBALS['TL_LANG']['MSC']['by'] . ' ' . $strAuthor;
					}
					break;

				case 'comments':
					if ($objArticle->noComments || !in_array('comments', \ModuleLoader::getActive()) || $objArticle->source != 'default')
					{
						break;
					}
					$intTotal = \CommentsModel::countPublishedBySourceAndParent('tl_news', $objArticle->id);
					$return['ccount'] = $intTotal;
					$return['comments'] = sprintf($GLOBALS['TL_LANG']['MSC']['commentCount'], $intTotal);
					break;
			}
		}

		return $return;
	}
}
