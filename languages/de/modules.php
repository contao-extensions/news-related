<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2024 Leo Feyer
 *
 * @package   contao-news-related
 * @author    Fast & Media | Christian Schmidt <dev@fast-media.net>
 * @license   LGPL
 * @copyright Fast & Media 2013-2024 <https://www.fast-media.net>
 */


/**
 * Front end modules
 */
$GLOBALS['TL_LANG']['FMD']['news_related'] = array('Ähnliche Nachrichten', 'Ähnliche Beiträge anzeigen');
