<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2014 Leo Feyer
 *
 * @package   contao-news-related
 * @author    Fast & Media | Christian Schmidt <dev@fast-media.net>
 * @license   LGPL
 * @copyright Fast & Media 2013-2014 <https://www.fast-media.net>
 */

/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_module']['related_numberOfItems'] = array('Number of similar entries','How many similar entries do you want to show up?');
$GLOBALS['TL_LANG']['tl_module']['related_template'] = array('Template for related news','Here you can select the template for related news.');


$GLOBALS['TL_LANG']['tl_module']['related_priority'] = array('Priority', 'Please select preferably diplayed posts. If you select "Relevance" the posts are sorted by the number of matches in descending order.');
$GLOBALS['TL_LANG']['tl_module']['related_priority']['random'] = 'Random';
$GLOBALS['TL_LANG']['tl_module']['related_priority']['relevance'] = 'Relevance (number of catchwords)';
$GLOBALS['TL_LANG']['tl_module']['related_priority']['date'] = 'Current posts';
$GLOBALS['TL_LANG']['tl_module']['related_priority']['comments'] = 'number of comments';


$GLOBALS['TL_LANG']['tl_module']['related_match'] = array('Matching criteria', 'Plese select by which criteria similar posts should be matched.');
$GLOBALS['TL_LANG']['tl_module']['related_match']['tags'] = 'Catchwords';
$GLOBALS['TL_LANG']['tl_module']['related_match']['category'] = 'News category';
$GLOBALS['TL_LANG']['tl_module']['related_match']['archive'] = 'News archive';

/**
 * Legends
 */
$GLOBALS['TL_LANG']['tl_module']['related_legend'] = 'Similar posts';
